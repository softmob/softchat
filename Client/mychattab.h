#ifndef MYCHATTAB_H
#define MYCHATTAB_H

#include <QWidget>

namespace Ui {
class MyChatTab;
}

class MyClient;

class MyChatTab : public QWidget
{
    Q_OBJECT
public:
    explicit MyChatTab(QWidget *parent = 0);
    MyChatTab(MyClient*, size_t, QWidget *parent = 0);
    void newMsg(const QString&, const QString&);
    void room_add(const QString&, const QString&);
    ~MyChatTab();
private slots:
    void on_pushButton_clicked();
    void on_add_clicked();
private:
    Ui::MyChatTab *ui;
    MyClient *chat;
    size_t room;
    QString name;
};

#endif // MYCHATTAB_H
