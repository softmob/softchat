#ifndef OPENCHAT_H
#define OPENCHAT_H

#include <QMainWindow>
#include <QStringList>
#include <map>

class MyClient;

namespace Ui {
class OpenChat;
}

class OpenChat : public QMainWindow
{
    Q_OBJECT
    
public:
    OpenChat(MyClient*, QWidget *parent = 0);
    ~OpenChat();
    void show_chat(QStringList&, size_t);
    void myNewMsg(size_t, const QString&, const QString&);
    void room_add(size_t, const QString&, const QString&);
    void rename(size_t);
private:
    Ui::OpenChat *ui;
    MyClient *chat;    
    std::map<QString, int> table;
    std::map<size_t, int> client_chat;
    std::map<int, size_t> chat_client;
};

#endif // OPENCHAT_H
