#include "myclient.h"

MyClient::MyClient(QObject *parent, QHostAddress host, quint16 port): QObject(parent)
{
    ui_login = new login(this);
    ui_setting = new Setting(this);
    ui_setting->hide();
    client = new QTcpSocket(this);
    client->connectToHost(host, port);
    connect(client, SIGNAL(connected()), this, SLOT(connect_to_srv()));
    connect(client, SIGNAL(disconnected()), this, SLOT(client_disconnect()));
    connect(client, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(clientError(QAbstractSocket::SocketError)));
    connect(client, SIGNAL(readyRead()), this, SLOT(getMessage()));
    connect(ui_login, SIGNAL(login_ok()), this, SLOT(start_chat()));
    connect(this, SIGNAL(signabout()), ui_login, SLOT(my_about()));
    connect(qApp, SIGNAL(lastWindowClosed()), this, SLOT(closed()));
    writeMapPoint();
    ui_login->show();
}

void MyClient::closed()
{
    setCmd(mode::status_off);
}

void MyClient::setting()
{
    ui_setting->show();
}

const QString& MyClient::get_name()
{
    return user.name;
}

void MyClient::show_chat(QStringList& list)
{
    ui_open_chat->show();
    ui_open_chat->show_chat(list, ++count);
    setCmd(mode::create_room, QSet<QString>::fromList(list), count);
}

void MyClient::fooRoomT(QDataStream& getm)
{
    size_t room, room_chat;
    getm >> room_chat >> room;
    room_sc[room] = room_chat;
    room_cs[room_chat] = room;
    user.room_list.insert(room);
    setCmd(mode::get_room_user, room);
}

void MyClient::start_chat()
{
    ui_login->hide();
    ui_chat_list= new ChatList(this);
    auto tit = ui_chat_list->windowTitle() + " - " +  user.name;
    ui_chat_list->setWindowTitle(tit);
    ui_chat_list->show();
    ui_open_chat = new OpenChat(this);
}

void MyClient::sentMsg(size_t room, const QString& str)
{
    room = room_cs[room];
    //qDebug() << "r:" << room << endl;
    setCmd(mode::new_meggase, room, str);
}

void MyClient::add_user(const QString& str)
{
    setCmd(mode::add_user, str);
}

void MyClient::add_user_yes(const QString& str)
{
    setCmd(mode::in_user_yes, str);
}

void MyClient::del_user(const QString& str)
{
    setCmd(mode::del_user, str);
}

void MyClient::create_room(const QSet<QString>& set)
{
    setCmd(mode::create_room, set);
}

void MyClient::add_in_room(size_t id, const QString& set)
{
    setCmd(mode::add_in_room, room_cs[id], set);
}

void setData(QDataStream&)
{}

template <typename T, typename... Args>
void setData(QDataStream& setm, T&& data, Args&&... args)
{
    setm << data;
    setData(setm, std::forward<Args>(args)...);
}

template <typename T, typename... Args>
void MyClient::setCmd(T&& val, Args&&... args)
{
    buf.clear();
    QDataStream setm(&buf, QIODevice::WriteOnly);
    setm << (quint16)0 << (quint16)val;
    setData(setm, std::forward<Args>(args)...);
    setm.device()->seek(0);
    setm << (quint16)(buf.size() - sizeof(quint16));
    client->write(buf);
}

void MyClient::authoriz(const QString& name, const QString& pass)
{
    user.name = name;
    setCmd(mode::login, name, pass);
    setCmd(mode::get_room_list);
    setCmd(mode::get_friend_list);
    setCmd(mode::get_user);
}

void MyClient::registration(const QString& name, const QString& pass)
{
    user.name = name;
    setCmd(mode::reg, name, pass);
    setCmd(mode::get_room_list);
    setCmd(mode::get_friend_list);
    setCmd(mode::get_user);
}

void MyClient::getMessage()
{
    if (client->bytesAvailable() > sizeof(quint16))
    {
        QDataStream getm(client);
        quint16 size_pack, cmd;
        getm >> size_pack;
        if (client->bytesAvailable() < size_pack)
            return;
        getm >> cmd;
        (this->*point_func[cmd])(getm);
        getMessage();
    }
}

void MyClient::writeMapPoint()
{
    point_func[mode::login_true] = &MyClient::fooLoginYes;
    point_func[mode::login_false] = &MyClient::fooLoginNo;
    point_func[mode::reg_true] = &MyClient::fooRegYes;
    point_func[mode::reg_false] = &MyClient::fooRegNo;
    point_func[mode::add_in_room_true] = &MyClient::fooAddInRoomTrue;
    point_func[mode::add_in_room_false] = &MyClient::fooAddInRoomFalse;
    point_func[mode::add_user_false] = &MyClient::fooAddUserFalse;
    point_func[mode::add_user_true] = &MyClient::fooAddUserTrue;
    point_func[mode::del_user_false] = &MyClient::fooDelUserFalse;
    point_func[mode::del_user_true] = &MyClient::fooDelUserTrue;
    point_func[mode::get_friend_list_in] = &MyClient::fooGetFriendListIn;
    point_func[mode::get_room_list_in] = &MyClient::fooGetRoomListIn;
    point_func[mode::get_room_user_in] = &MyClient::fooGetRoomUserIn;
    point_func[mode::new_in_meggase] = &MyClient::fooNewInMeggase;
    point_func[mode::room_t] = &MyClient::fooRoomT;
    point_func[mode::in_user] = &MyClient::fooInUser;
    point_func[mode::status] = &MyClient::fooStatus;
    point_func[mode::room_add] = &MyClient::fooRoomAdd;
    point_func[mode::get_user_in] = &MyClient::fooGetUserIn;
}

const QSet<QString>& MyClient::get_friend_list()
{
    //qDebug() << user.room_list.size();
    return user.friend_list;
}

const QSet<QString>& MyClient::get_room_list(size_t ind)
{
    return user.room_user[room_cs[ind]];
}

const QSet<QString>& MyClient::get_room_list2(size_t ind)
{
    return user.room_user[ind];
}

const QSet<size_t>& MyClient::get_room()
{
    return user.room_list;
}

const users& MyClient::get_user()
{
    return user.personal;
}


void MyClient::fooRoomAdd(QDataStream& getm)
{
    QString who, whom;
    size_t room;
    getm >> room >> who >> whom;
    if (room_sc.find(room) == room_sc.cend())
    {
        room_sc[room] = ++count;
        room_cs[count] = room;
    }
    user.room_user[room].insert(whom);
    ui_open_chat->room_add(room_sc[room], who, whom);
}

void MyClient::fooStatus(QDataStream& getm)
{
    bool f;
    QString name;
    getm >> name >> f;
    ui_chat_list->setStatus(name, f);
}

void MyClient::fooInUser(QDataStream& getm)
{
    QString name;
    getm >> name;
    ui_chat_list->in_user(name);
}

void MyClient::fooLoginYes(QDataStream&)
{

    emit authoriz_yes();
}

void MyClient::fooLoginNo(QDataStream&)
{
    emit authoriz_no();
}

void MyClient::fooRegYes(QDataStream&)
{

    emit reg_yes();
}

void MyClient::fooRegNo(QDataStream&)
{
    emit reg_no();
}

void MyClient::fooAddInRoomTrue(QDataStream& gets)
{
    size_t room;
    QString str;
    gets >> room >> str;
    emit signalAddInRoomTrue(room, str);
}

void MyClient::fooAddInRoomFalse(QDataStream& gets)
{
    size_t room;
    QString str;
    gets >> room >> str;
    emit signalAddInRoomFalse(room, str);
}

void MyClient::fooAddUserFalse(QDataStream& gets)
{
    QString str;
    gets >> str;
    emit signalAddUserFalse(str);
}

void MyClient::fooAddUserTrue(QDataStream& gets)
{
    QString str;
    gets >> str;
    user.friend_list.insert(str);
    emit signalAddUserTrue(str);
}

void MyClient::fooDelUserFalse(QDataStream& gets)
{
    QString str;
    gets >> str;
    emit signalDelUserFalse(str);
}

void MyClient::fooDelUserTrue(QDataStream& gets)
{
    QString str;
    gets >> str;
    emit signalDelUserTrue(str);
}

void MyClient::fooGetFriendListIn(QDataStream& gets)
{
    gets >> user.friend_list;
    emit load_friend();
}

void MyClient::fooGetRoomListIn(QDataStream& gets)
{
    gets >> user.room_list;
    for (auto &x: user.room_list)
        setCmd(mode::get_room_user, (size_t)x);
    emit load_chat_save();
}

void MyClient::fooGetRoomUserIn(QDataStream& gets)
{
    size_t room;
    gets >> room;
    gets >> user.room_user[room];
    user.room_user[room].remove(user.name);
    //ui_open_chat->rename(room_sc[room]);
    emit load_chat(room);
}

void MyClient::fooGetUserIn(QDataStream& gets)
{
    gets >> user.personal;
    /* UserEdit *tmp = new UserEdit(this);
    tmp->show();*/
}

void MyClient::fooNewInMeggase(QDataStream& gets)
{
    size_t room;
    QString from, msg;
    gets >> room >> from >> msg;
    //qDebug() <<  user.name << " ot " << from << ' ' << msg << endl;
    if (room_sc.find(room) == room_sc.cend())
    {
        room_sc[room] = ++count;
        room_cs[count] = room;
    }
    ui_open_chat->myNewMsg(room_sc[room], from, msg);
}

void MyClient::connect_to_srv()
{
    return ;
}

void MyClient::clientError(QAbstractSocket::SocketError)
{   
    client->close();
    if (client->error() == QAbstractSocket::ConnectionRefusedError)
        client->connectToHost(QHostAddress::LocalHost, 1234);
    else if (client->error() == QAbstractSocket::RemoteHostClosedError)
    {
        QMessageBox::critical(ui_chat_list, "Error", "Сервер закрыл соединение");
        qApp->exit();
    }
}

void MyClient::client_disconnect()
{
    client->close();
}
