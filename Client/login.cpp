#include "login.h"
#include "ui_login.h"
#include "myclient.h"

login::login(MyClient *oth, QWidget *parent) :
    QMainWindow(parent), ui(new Ui::login), chat(oth)
{
    ui->setupUi(this);
    ui->logo->setPixmap(QPixmap(":/images/chat.png"));
    ui->logo->setScaledContents(true);
    setFixedSize(size());
    ui->exit->setIcon(QPixmap(":/images/application-exit"));
    //ui->setting->setIcon(QPixmap(":/images/preferences-other.png"));
    ui->about->setIcon(QPixmap(":/images/help-contents"));
    connect(ui->name, SIGNAL(activated(const QString&)), ui->name->lineEdit(), SLOT(selectAll()));
    connect(ui->name->lineEdit(), SIGNAL(returnPressed()), this, SLOT(my_enter()));
    connect(ui->pass, SIGNAL(returnPressed()), this, SLOT(my_enter()));
    connect(ui->about, SIGNAL(triggered()), this, SLOT(my_about()));
    connect(ui->setting, SIGNAL(triggered()), chat, SLOT(setting()));
    connect(chat, SIGNAL(authoriz_no()), this, SLOT(my_authoriz_no()));
    connect(chat, SIGNAL(authoriz_yes()), this, SLOT(my_authoriz_yes()));
    connect(chat, SIGNAL(reg_no()), this, SLOT(my_reg_no()));
    connect(chat, SIGNAL(reg_yes()), this, SLOT(my_reg_yes()));
    connect(ui->exit, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));
    //ui->name->addItem("andrej");
}

void login::start_chat()
{
     emit login_ok();
}

void login::my_authoriz_yes()
{
    start_chat();
}

void login::my_authoriz_no()
{
    QMessageBox::critical(this, tr("Ошибка авторизации"), tr("Неверный пароль или логин"));
}

void login::my_reg_yes()
{
    start_chat();
}

void login::my_reg_no()
{
    QMessageBox::warning(this, tr("Ошибка регистрации"), tr("Данное имя уже используется"));
}

void login::my_enter()
{
    if (!ui->name->lineEdit()->text().isEmpty() &&!ui->pass->text().isEmpty())
    {
        emit ui->authorization->clicked();
    }
}

login::~login()
{
    delete ui;
}

void login::my_about()
{
    QWidget tmp(this);
    tmp.setWindowIcon(QPixmap(":/images/help-contents"));
    QMessageBox::about(&tmp, tr("О SoftChat"),
                       tr("<p>Клиент Серверный Чат"
                          "<p>Проект разработан в рамках SummerCode'13"
                          "<p>Автор: Наумов Андрей"
                          ));
}

void login::on_authorization_clicked()
{
    if (ui->name->lineEdit()->text().isEmpty() || ui->pass->text().isEmpty())
        return;
    chat->authoriz(ui->name->lineEdit()->text(), ui->pass->text());
}

void login::on_registration_clicked()
{
    if (ui->name->lineEdit()->text().isEmpty() || ui->pass->text().isEmpty())
        return;
    chat->registration(ui->name->lineEdit()->text(), ui->pass->text());
}
