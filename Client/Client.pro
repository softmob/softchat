#-------------------------------------------------
#
# Project created by QtCreator 2013-07-08T21:34:10
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Client
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++0x

SOURCES += main.cpp\
        login.cpp \
    myclient.cpp \
    chatlist.cpp \
    openchat.cpp \
    userdata.cpp \
    mychattab.cpp \
    setting.cpp \
    useredit.cpp

HEADERS  += login.h \
    myclient.h \
    mode.h \
    chatlist.h \
    openchat.h \
    user.h \
    userdata.h \
    mychattab.h \
    setting.h \
    useredit.h

FORMS    += login.ui \
    chatlist.ui \
    openchat.ui \
    mychattab.ui \
    setting.ui \
    useredit.ui

RESOURCES += \
    resources.qrc
