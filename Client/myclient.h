#ifndef MYCLIENT_H
#define MYCLIENT_H

#include <QtNetwork>
#include "mode.h"
#include "login.h"
#include "chatlist.h"
#include "userdata.h"
#include "openchat.h"
#include "setting.h"
#include "useredit.h"
#include "user.h"

class MyClient: public QObject
{
    Q_OBJECT
public:
    MyClient(QObject *parent = nullptr,
             QHostAddress host = QHostAddress::LocalHost, quint16 port = 1234);
    void authoriz(const QString&, const QString&);
    void registration(const QString&, const QString&);
    const QSet<QString>& get_friend_list();
    const QSet<QString>& get_room_list(size_t);
    const QSet<QString>& get_room_list2(size_t);
    const QSet<size_t>& get_room();
    const users& get_user();
    const QString& get_name();
    void add_user(const QString&);
    void add_user_yes(const QString&);
    void del_user(const QString&);
    void create_room(const QSet<QString>&);
    void add_in_room(size_t, const QString&);
    void show_chat(QStringList&);
    void sentMsg(size_t, const QString&);
public slots:
    void setting();
private:
    void writeMapPoint();
    void fooRoomAdd(QDataStream&);
    void fooStatus(QDataStream&);
    void fooLoginYes(QDataStream&);
    void fooLoginNo(QDataStream&);
    void fooRegYes(QDataStream&);
    void fooRegNo(QDataStream&);
    void fooAddInRoomTrue(QDataStream&);
    void fooAddInRoomFalse(QDataStream&);
    void fooAddUserFalse(QDataStream&);
    void fooAddUserTrue(QDataStream&);
    void fooDelUserFalse(QDataStream&);
    void fooDelUserTrue(QDataStream&);
    void fooGetFriendListIn(QDataStream&);
    void fooGetRoomListIn(QDataStream&);
    void fooGetRoomUserIn(QDataStream&);    
    void fooGetUserIn(QDataStream&);
    void fooNewInMeggase(QDataStream&);
    void fooRoomT(QDataStream&);
    void fooInUser(QDataStream&);
    template <typename T, typename... Args>
    void setCmd(T&&, Args&&...);
private slots:
    void closed();
    void start_chat();
    void connect_to_srv();
    void getMessage();
    void clientError(QAbstractSocket::SocketError);
    void client_disconnect();
private:
    std::map<quint16, void(MyClient::*)(QDataStream&)> point_func;
    QByteArray buf;
    QTcpSocket *client;
    login *ui_login;
    ChatList *ui_chat_list;
    OpenChat *ui_open_chat;
    Setting *ui_setting;
    UserEdit *ui_user_edit;
    UserData user;
    size_t count;
    std::map<size_t, size_t> room_sc, room_cs;
signals:
    void authoriz_yes();
    void authoriz_no();
    void reg_yes();
    void reg_no();
    void load_friend();
    void load_chat(size_t);
    void load_chat_save();   
    void signalAddInRoomTrue(size_t, QString);
    void signalAddInRoomFalse(size_t, QString);
    void signalAddUserFalse(QString);
    void signalAddUserTrue(QString);
    void signalDelUserFalse(QString);
    void signalDelUserTrue(QString);
    void signabout();
};

#endif // MYCLIENT_H
