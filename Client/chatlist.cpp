#include "chatlist.h"
#include "ui_chatlist.h"
#include <QStringListModel>
#include <QtWidgets>
#include <QList>
#include "myclient.h"

ChatList::ChatList(MyClient *oth, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ChatList), chat(oth), count_list_chat(0)
{
    ui->setupUi(this);
    ui->listWidget->setSortingEnabled(true);
    connect(chat, SIGNAL(signalAddInRoomTrue(size_t, QString)), this, SLOT(myAddInRoomTrue(size_t, QString)));
    connect(chat, SIGNAL(signalAddInRoomFalse(size_t, QString)), this, SLOT(myAddInRoomFalse(size_t, QString)));
    connect(chat, SIGNAL(signalAddUserFalse(QString)), this, SLOT(myAddUserFalse(QString)));
    connect(chat, SIGNAL(signalAddUserTrue(QString)), this, SLOT(myAddUserTrue(QString)));
    connect(chat, SIGNAL(signalDelUserFalse(QString)), this, SLOT(myDelUserFalse(QString)));
    connect(chat, SIGNAL(signalDelUserTrue(QString)), this, SLOT(myDelUserTrue(QString)));
    connect(chat, SIGNAL(load_friend()), this, SLOT(view_list()));
    connect(chat, SIGNAL(load_chat(size_t)), this, SLOT(chat_view(size_t)));
    connect(ui->add_user, SIGNAL(triggered()), this, SLOT(add_user()));
    connect(ui->about, SIGNAL(triggered()), chat, SIGNAL(signabout()));
    connect(ui->setting, SIGNAL(triggered()), chat, SLOT(setting()));
    connect(ui->in_conf, SIGNAL(triggered()), this, SLOT(in_conf()));
    //connect(ui-, SIGNAL(triggered()), this, SLOT(in_conf()));
    ui->in_conf->setIcon(QPixmap(":/images/list-add.png"));
    ui->add_user->setIcon(QPixmap(":/images/contact-new.png"));
    // ui->user_edit->setIcon(QPixmap(":/images/edit-clear.png"));
    ui->exit->setIcon(QPixmap(":/images/application-exit"));
    //ui->setting->setIcon(QPixmap(":/images/preferences-other.png"));
    ui->about->setIcon(QPixmap(":/images/help-contents"));
    ui->tabWidget->setTabIcon(0, QPixmap(":/images/x-office-address-book.png"));
    ui->tabWidget->setTabIcon(1, QPixmap(":/images/internet-group-chat.png"));
    connect(ui->exit, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));
}

void ChatList::in_conf()
{
    QWidget tmp(this);
    tmp.setWindowIcon(QPixmap(":/images/list-add.png"));
    QString add = QInputDialog::getText(&tmp, tr("Начать новый чат"), tr("Введите имена пользователей \nв формате 'name1,name2': "));
    if (!add.isEmpty() && add != QString(add.size(), ' '))
    {
        QStringList list = add.split(",");
        for (auto &x: list)
        {
            if (chat->get_friend_list().find(x) == chat->get_friend_list().cend())
            {
                QMessageBox::critical(this, tr("Создание чата"),
                                      x + " - Пользователя нет в вашем списке контактов");
                return;
            }
        }
        chat->show_chat(list);
    }
}

void ChatList::chat_view(size_t room)
{    
    auto tmp = chat->get_room_list2(room);
    tmp.remove(chat->get_name());
    QStringList tmpl = tmp.toList();
    auto str = tmpl.join(",");
    if (tmpl.size() > 1 && !str.isEmpty())
    {
        if (chats.find(room) == chats.cend())
        {
            ui->listWidget_2->addItem(str);
            chats[room] = count_list_chat++;
        }
        else
        {
            ui->listWidget_2->setCurrentRow(chats[room]);
            ui->listWidget_2->currentItem()->setText(str);
        }
    }
}

void ChatList::view_list()
{
    ui->listWidget->clear();
    friends = chat->get_friend_list();
    for (auto &x: friends)
    {
        QListWidgetItem *tmp = new QListWidgetItem(QPixmap(":images/status-offline.png"), x);
        ui->listWidget->addItem(tmp);
        item[x] = tmp;
    }
}

void ChatList::myAddInRoomTrue(size_t room, QString str)
{
    //QMessageBox::information(this, tr("Добавление в чат"), str + " - Пользователь присоединет");
}

void ChatList::myAddInRoomFalse(size_t room, QString str)
{
    QMessageBox::critical(this, tr("Добавление в чат"), str + " - Пользователь не найден");
}

void ChatList::myAddUserFalse(QString str)
{
    QMessageBox::critical(this, tr("Добавление контакта"), str + " - Пользователь не найден");
}

void ChatList::myAddUserTrue(QString str)
{

    if (friends.find(str) == friends.cend())
    {
        QListWidgetItem *tmp = new QListWidgetItem(QPixmap(":images/status-offline.png"), str);
        ui->listWidget->addItem(tmp);
        item[str] = tmp;
        friends.insert(str);
    }
    else
    {
        QMessageBox::warning(this, tr("Добавление контакта"), "Пользователь уже был добавлен ранее");
    }
}

void ChatList::myDelUserFalse(QString str)
{
    QMessageBox::critical(this, tr("Удаление контакт"), str + " - Пользователь не найден: ");
}

void ChatList::myDelUserTrue(QString str)
{
    //ui->ddget->takeItem(str);
    QMessageBox::information(this, tr("Удаление контакта"), str + " - Пользователь успешно удален");
}

ChatList::~ChatList()
{
    delete ui;
}

void ChatList::add_user()
{
    QWidget tmp(this);
    tmp.setWindowIcon(QPixmap(":/images/contact-new.png"));
    QString add = QInputDialog::getText(&tmp, tr("Добавление пользователя"), tr("Введите имя пользователя: "));
    if (add == chat->get_name())
        QMessageBox::critical(this, tr("Добавление контакта"), "Вы не можете добавить себя в свой список контактов!)");
    else if (friends.find(add) != friends.cend())
    {
        QMessageBox msgb(this);
        msgb.setWindowIcon(QPixmap(":/images/contact-new.png"));
        msgb.setIcon(QMessageBox::Warning);
        msgb.setWindowTitle(tr("Запрос на добавление"));
        msgb.setText("Пользователь "
                     + add + " уже в список контактов.");
        msgb.exec();
    }
    else if (!add.isEmpty())
        chat->add_user(add);
}

void ChatList::setStatus(const QString& name, bool st)
{
    const char *str = !st ? ":images/status-offline.png" : ":images/status.png";
    item[name]->setIcon(QPixmap(str));
}

void ChatList::on_listWidget_doubleClicked(const QModelIndex &index)
{
    QStringList list;
    list.append(ui->listWidget->item(index.row())->text());
    chat->show_chat(list);
}

void ChatList::on_listWidget_2_clicked(const QModelIndex &index)
{
    QStringList list;
    list.append(ui->listWidget_2->item(index.row())->text());
    chat->show_chat(list);
}

void ChatList::on_listWidget_2_doubleClicked(const QModelIndex &index)
{
    on_listWidget_2_clicked(index);
}

void ChatList::on_listWidget_customContextMenuRequested(const QPoint &pos)
{
    /*QMessageBox::information(this, tr("Удаление контакта"),  " - Пользователь успешно удален");
    QMenu *menu = new QMenu(this);
    QAction *act = new QAction(menu);
    act->setText(tr("Открыть"));
    connect(act,SIGNAL(triggered()), this, SLOT(openAct()));
    menu->addAction(act);*/
}

void ChatList::in_user(const QString& from)
{
    QMessageBox msgb(this);
    msgb.setWindowIcon(QPixmap(":/images/contact-new.png"));
    msgb.setWindowTitle(tr("Запрос на добавление"));
    msgb.setText("Добавить пользователя "
                 + from + " в список контактов?");
    msgb.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
    if (msgb.exec() == QMessageBox::Ok)
    {
        chat->add_user_yes(from);
        myAddUserTrue(from);
    }
}

void ChatList::openAct()
{

}
