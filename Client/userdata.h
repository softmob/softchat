#ifndef USERDATA_H
#define USERDATA_H
#include "user.h"
#include <QSet>
#include <map>

struct UserData
{
    UserData();
    QSet<QString> friend_list;
    QSet<size_t> room_list;
    std::map<size_t, QSet<QString>> room_user;
    users personal;
    QString name;
};

#endif // USERDATA_H
