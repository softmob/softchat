#include "useredit.h"
#include "ui_useredit.h"
#include "myclient.h"
#include "user.h"

UserEdit::UserEdit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserEdit)
{
    ui->setupUi(this);
}

UserEdit::UserEdit(MyClient *oth, QWidget *parent) :chat(oth),
    QWidget(parent),
    ui(new Ui::UserEdit)
{
    ui->setupUi(this);
    ui->lineEdit->setText(chat->get_user().name);
    ui->lineEdit_2->setText(QString::number(chat->get_user().age));
    ui->comboBox->setCurrentIndex(chat->get_user().gender);
}

UserEdit::~UserEdit()
{
    delete ui;
}
