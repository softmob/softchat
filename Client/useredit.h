#ifndef USEREDIT_H
#define USEREDIT_H

#include <QWidget>

namespace Ui {
class UserEdit;
}

class MyClient;

class UserEdit : public QWidget
{
    Q_OBJECT
    
public:
    explicit UserEdit(QWidget *parent = 0);
    UserEdit(MyClient *, QWidget *parent = 0);
    ~UserEdit();
    
private:
    Ui::UserEdit *ui;
    MyClient *chat;
};

#endif // USEREDIT_H
