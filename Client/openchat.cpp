#include "openchat.h"
#include "ui_openchat.h"
#include "myclient.h"
#include "ui_mychattab.h"

OpenChat::OpenChat(MyClient *oth, QWidget *parent) : chat(oth),
    QMainWindow(parent),
    ui(new Ui::OpenChat)
{
    //qDebug() << "ok\n" << flush;
    ui->setupUi(this);
    ui->tabWidget->clear();
}

void OpenChat::show_chat(QStringList& list, size_t room)
{
    show();
    auto str = list.join(",");
    if (table.find(str) == table.end())
    {
        MyChatTab *tab = new MyChatTab(chat, room, ui->tabWidget);
        int curr = ui->tabWidget->addTab(tab, str);
        table[str] = curr;
        client_chat[room] = curr;
        chat_client[curr] = room;
        ui->tabWidget->setCurrentIndex(curr);
    }
    else
        ui->tabWidget->setCurrentIndex(table[str]);
    activateWindow();
    //qDebug() << room << endl;
}

void OpenChat::rename(size_t room)
{
    return;
    if (client_chat.find(room) != client_chat.cend())
    {
        //qDebug() << "qw";
        int curr = client_chat[room];
        QStringList list = chat->get_room_list(room).toList();
        ui->tabWidget->setTabText(curr, list.join(","));
    }
}

void OpenChat::room_add(size_t room, const QString& who, const QString& whom)
{
    show();
    auto tmp = chat->get_room_list(room);
    tmp.remove(chat->get_name());
    QStringList list = tmp.toList();
    if (client_chat.find(room) == client_chat.cend())
    {
        show_chat(list, room);
    }
    int ch = client_chat[room];
    table[list.join(",")] = ch;
    ui->tabWidget->setCurrentIndex(ch);
    ui->tabWidget->setTabText(ch, list.join(","));
    MyChatTab *tab =  (MyChatTab*)ui->tabWidget->currentWidget();
    tab->room_add(who, whom);
    QApplication::alert(this);
}

OpenChat::~OpenChat()
{
    delete ui;
}

void OpenChat::myNewMsg(size_t room, const QString& from, const QString& str)
{
    show();
    if (client_chat.find(room) == client_chat.cend())
    {
        QStringList list = chat->get_room_list(room).toList();
        show_chat(list, room);
    }
    int ch = client_chat[room];
    ui->tabWidget->setCurrentIndex(ch);
    MyChatTab *tab =  (MyChatTab*)ui->tabWidget->currentWidget();
    tab->newMsg(from, str);
    QApplication::alert(this);
}
