#include "setting.h"
#include "ui_setting.h"
#include "myclient.h"

Setting::Setting(MyClient * oth, QWidget *parent) : chat(oth),
    QMainWindow(parent),
    ui(new Ui::Setting)
{
    ui->setupUi(this);
}

Setting::~Setting()
{
    delete ui;
}
