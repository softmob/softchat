#ifndef MODE_H
#define MODE_H
namespace mode
{
enum
{
    login,
    login_true,
    login_false,
    reg,
    reg_true,
    reg_false,
    add_user,
    del_user,
    create_room,
    add_in_room,
    add_in_room_true,
    add_in_room_false,
    add_user_true,
    add_user_false,
    del_user_true,
    del_user_false,
    new_meggase,
    new_in_meggase,
    get_friend_list,
    get_friend_list_in,
    get_room_list,
    get_room_list_in,
    get_room_user,
    get_room_user_in,
    room_t,
    in_user,
    in_user_yes,
    status,
    room_add,
    get_user,
    get_user_in,
    set_user,
    status_off
};
}
#endif // MODE_H
