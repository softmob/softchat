#ifndef SETTING_H
#define SETTING_H

#include <QMainWindow>

namespace Ui {
class Setting;
}

class MyClient;

class Setting : public QMainWindow
{
    Q_OBJECT
    
public:
    Setting(MyClient *oth, QWidget *parent = 0);
    ~Setting();
    
private:
    Ui::Setting *ui;
    MyClient *chat;
};

#endif // SETTING_H
