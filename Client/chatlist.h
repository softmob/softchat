#ifndef CHATLIST_H
#define CHATLIST_H

#include <QMainWindow>
#include <QSet>
#include <QString>
#include <map>
#include <QListWidgetItem>

namespace Ui {
class ChatList;
}

class MyClient;

class ChatList : public QMainWindow
{
    Q_OBJECT
    
public:
    ChatList(MyClient *oth, QWidget *parent = nullptr);
    ~ChatList();
    void in_user(const QString&);
    void setStatus(const QString&, bool);
private slots:
    void in_conf();
    void view_list();
    void chat_view(size_t);
    void myAddInRoomTrue(size_t, QString);
    void myAddInRoomFalse(size_t, QString);
    void myAddUserFalse(QString);
    void myAddUserTrue(QString);
    void myDelUserFalse(QString);
    void myDelUserTrue(QString);
    void add_user();
    void on_listWidget_doubleClicked(const QModelIndex &index);
    void on_listWidget_2_clicked(const QModelIndex &index);
    void on_listWidget_2_doubleClicked(const QModelIndex &index);
    void on_listWidget_customContextMenuRequested(const QPoint &pos);
    void openAct();
private:
    QSet<QString> friends;
    Ui::ChatList *ui;
    MyClient *chat;
    std::map<QString, QListWidgetItem *> item;
    std::map<size_t, int> chats;
    int count_list_chat;
};

#endif // CHATLIST_H
