#ifndef LOGIN_H
#define LOGIN_H

#include <QMainWindow>
#include <QtWidgets>

namespace Ui {
class login;
}

class MyClient;

class login : public QMainWindow
{
    Q_OBJECT
public:
    login(MyClient *oth, QWidget *parent = nullptr);
    ~login();
public slots:
     void my_about();
signals:
    void login_ok();
private slots:
    void my_authoriz_yes();
    void my_authoriz_no();
    void my_reg_yes();
    void my_reg_no();
    void my_enter();
    void on_authorization_clicked();
    void on_registration_clicked();
private:
    void start_chat();
private:
    Ui::login *ui;
    MyClient *chat;
};

#endif // LOGIN_H
