#include "mychattab.h"
#include "ui_mychattab.h"
#include <QDateTime>
#include <QScrollBar>
#include "myclient.h"
#include <QCursor>

MyChatTab::MyChatTab(QWidget *parent) :
    QWidget(parent), ui(new Ui::MyChatTab)
{
    ui->setupUi(this);
}

MyChatTab::MyChatTab(MyClient *oth, size_t r, QWidget *parent) : chat(oth), room(r),
    QWidget(parent),
    ui(new Ui::MyChatTab)
{
    ui->setupUi(this);
    name = chat->get_name();
}

MyChatTab::~MyChatTab()
{
    delete ui;
}

void MyChatTab::on_pushButton_clicked()
{
    const QString& str = ui->plainTextEdit->document()->toPlainText();
    if (str.isEmpty())
        return;
    ui->textBrowser->moveCursor(QTextCursor::End);
    ui->textBrowser->insertHtml(
                "<img src=':images/go-previous.png'/>" +
                name + " (" +
                QTime::currentTime().toString()+ "):<br>" +
                str+"<br>");
    ui->textBrowser->scrollBarWidgets(Qt::AlignBottom);
    ui->textBrowser->verticalScrollBar()->setValue(ui->textBrowser->verticalScrollBar()->maximum());
    ui->plainTextEdit->clear();
    chat->sentMsg(room, str);
}

void MyChatTab::room_add(const QString& who, const QString& whom)
{
    ui->textBrowser->moveCursor(QTextCursor::End);
    ui->textBrowser->insertHtml(
                "<img src=':images/list-add.png'/>" +
                who + " присоединил к чату "+
                whom + "<br>");
    ui->textBrowser->verticalScrollBar()->setValue(ui->textBrowser->verticalScrollBar()->maximum());
}

void MyChatTab::newMsg(const QString& from, const QString& str)
{
    auto scr = ui->textBrowser->verticalScrollBar()->value();
    ui->textBrowser->moveCursor(QTextCursor::End);
    ui->textBrowser->insertHtml(
                "<img src=':images/go-next.png'/>" +
                from + " (" +
                QTime::currentTime().toString()+ "):<br>" +
                str+"<br>");
    ui->textBrowser->verticalScrollBar()->setValue(scr);
}

void MyChatTab::on_add_clicked()
{  
    QString add = QInputDialog::getText(this, tr("Добавление пользователя к чату"),
                                        tr("Введите имя пользователя: "));
    if (chat->get_friend_list().find(add) == chat->get_friend_list().cend())
    {
        QMessageBox::critical(this, tr("Добавление пользователя к чату"),
                              add + " - Пользователя нет в вашем списке контактов");
    }
    else if (!add.isEmpty())
        chat->add_in_room(room, add);
}
