#-------------------------------------------------
#
# Project created by QtCreator 2013-07-09T18:56:16
#
#-------------------------------------------------

QT       += core
QT       += network
QT       -= gui

TARGET = Server
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++0x

SOURCES += main.cpp \
    control.cpp \
    myserver.cpp

HEADERS += \
    mode.h \
    user.h \
    control.h \
    myserver.h
