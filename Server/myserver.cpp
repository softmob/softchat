#include "myserver.h"

MyServer::MyServer(QObject *parent, QHostAddress type_addr, quint16 port):
    QObject(parent), host(type_addr), srv_port(port)
{
    server = new QTcpServer(this);
    if (server->listen(host, port))
    {
        out << "Server is started." << endl << flush;
        out << "Address: " << type_addr.toString() << " port: " << port << endl << flush;
        connect(server,SIGNAL(newConnection()),this, SLOT(connection()));
    }
    else
    {
        out << server->errorString() << endl << flush;
    }
    writeMapPoint();
    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(close()));
}

void MyServer::close()
{
    for (auto &x: table)
    {
        from = x.second;
        if (from->isOpen())
            from->close();
    }
    server->close();
}

void MyServer::connection()
{
    from = server->nextPendingConnection();
    table[from->socketDescriptor()] = from;
    out << "User connected" << endl << flush;
    connect(from, SIGNAL(readyRead()), this, SLOT(getMessage()));
 }

void MyServer::fooUserDisconnect(QDataStream& getm)
{
    auto str = login[from->socketDescriptor()] ;
    out <<"User Disconnect: " << str << ' ' << from->socketDescriptor() << endl << flush;
    status[str] = false;
    setstatus(str);
}

void MyServer::setstatus(const QString& str)
{
    for (auto &x: data.get_user_list_friend(str))
    {
        if (socket_login.find(x) != socket_login.cend())
        {
            from = table[socket_login[x]];
            setCmd(mode::status, str, status[str]);
        }
    }
}

void MyServer::getMessage()
{
    from = (QTcpSocket*)sender();
    if (from->bytesAvailable() > sizeof(quint16))
    {
        QDataStream getm(from);
        quint16 size_pack, cmd;
        getm >> size_pack;
        if (from->bytesAvailable() < size_pack)
            return;
        getm >> cmd;
        //out << "Request from: " << login[from->socketDescriptor()] << " number: " << cmd << endl << flush;;
        (this->*point_func[cmd])(getm);
        getMessage();
    }
}

void MyServer::writeMapPoint()
{
    point_func[mode::new_meggase] = &MyServer::fooNewMessage;
    point_func[mode::login] = &MyServer::fooLogin;
    point_func[mode::reg] = &MyServer::fooRegistration;
    point_func[mode::create_room] = &MyServer::fooNewRoom;
    point_func[mode::add_in_room] = &MyServer::fooAddRoom;
    point_func[mode::add_user] = &MyServer::fooAddUser;
    point_func[mode::del_user] = &MyServer::fooDelUser;
    point_func[mode::get_room_list] = &MyServer::fooGetRoomList;
    point_func[mode::get_friend_list] = &MyServer::fooGetFriendList;
    point_func[mode::get_room_user] = &MyServer::fooGetRoomUser;
    point_func[mode::in_user_yes] = &MyServer::fooAddUserYes;
    point_func[mode::get_user] = &MyServer::fooGetUser;
    point_func[mode::set_user] = &MyServer::fooSetUser;
    point_func[mode::status_off] = &MyServer::fooUserDisconnect;
}

void MyServer::fooLogin(QDataStream& getm)
{
    QString name, pass;
    getm >> name >> pass;
    quint16 res = (data.user_login(name, pass)) ? mode::login_true : mode::login_false;
    setCmd(res);
    login[from->socketDescriptor()] = name;
    socket_login[name] = from->socketDescriptor();
    status[name] = true;
    if (res == mode::login_true)
    {
        setstatus(name);
        out << "the user has successfully logged: " << name << ' ' << socket_login[name] << endl << flush;
    }
}

void MyServer::fooRegistration(QDataStream& getm)
{
    QString name, pass;
    getm >> name >> pass;
    quint16 res = (data.user_create(name, pass)) ? mode::reg_true : mode::reg_false;
    setCmd(res);
    login[from->socketDescriptor()] = name;
    socket_login[name] = from->socketDescriptor();
    status[name] = true;
    if (res == mode::reg_true)
    {
        setstatus(name);
        out << "the user has successfully registered: " << name << ' ' << socket_login[name] << endl << flush;
    }
}

void MyServer::fooNewRoom(QDataStream& getm)
{
    size_t r;
    QSet<QString> list;
    getm >> list >> r;
    list.insert(login[from->socketDescriptor()]);
    size_t room = data.room_create(list);
    setCmd(mode::room_t, r, room);
    for (auto &x: data.get_list_room(room))
    {
        if (socket_login.find(x) != socket_login.cend())
        {
            from = table[socket_login[x]];
            setCmd(mode::get_room_user_in, room, data.get_list_room(room));
        }
    }
    out << "new room number: " << room << endl << flush;
}

void MyServer::fooAddRoom(QDataStream& getm)
{
    size_t room;
    QString str;
    getm >> room >> str;
    quint16 res = (data.room_add(room, str))
            ? mode::add_in_room_true :  mode::add_in_room_false;
    setCmd(res, room, str);
    QString& name = login[from->socketDescriptor()];
    if (res == mode::add_in_room_true)
    {
        out << "room number: " << room << " added " << str << endl << flush;
        for (auto &x: data.get_list_room(room))
        {
            if (socket_login.find(x) != socket_login.cend())
            {
                from = table[socket_login[x]];
                setCmd(mode::get_room_user_in, room, data.get_list_room(room));
                setCmd(mode::room_add, room, name, str);
            }
        }
    }
}

void MyServer::fooAddUser(QDataStream& getm)
{
    QString whom;
    getm >> whom;
    quint16 res = (data.user_add(login[from->socketDescriptor()], whom))
            ? mode::add_user_true :  mode::add_user_false;
    setCmd(res, whom);
    if (res == mode::add_user_true && socket_login.find(whom) != socket_login.cend())
    {
        out << login[from->socketDescriptor()] << " added " << whom << endl << flush;
        setCmd(mode::status, whom, status[whom]);
        auto str = login[from->socketDescriptor()];
        from = table[socket_login[whom]];
        setCmd(mode::in_user, str);
    }
}

void MyServer::fooAddUserYes(QDataStream& getm)
{
    QString whom;
    getm >> whom;
    out << login[from->socketDescriptor()] << " added " << whom << endl << flush;
    data.user_add(login[from->socketDescriptor()], whom);
    setCmd(mode::status, whom, status[whom]);
}

void MyServer::fooDelUser(QDataStream& getm)
{
    QString whom;
    getm >> whom;
    quint16 res = (data.user_del(login[from->socketDescriptor()], whom))
            ? mode::del_user_true :  mode::del_user_false;
    setCmd(res, whom);
}

void MyServer::fooGetRoomList(QDataStream& getm)
{
    setCmd(mode::get_room_list_in, data.get_user_list_room(login[from->socketDescriptor()]));
}

void MyServer::fooGetFriendList(QDataStream& getm)
{
    QSet<QString> & list = data.get_user_list_friend(login[from->socketDescriptor()]);
    setCmd(mode::get_friend_list_in, list);
    for (auto &x: list)
    {
        if (socket_login.find(x) != socket_login.cend())
        {
            setCmd(mode::status, x, status[x]);
        }
    }
}

void MyServer::fooGetUser(QDataStream& getm)
{
    setCmd(mode::get_user_in, data.get_user(login[from->socketDescriptor()]));
}

void MyServer::fooSetUser(QDataStream& getm)
{
    user tmp;
    getm >> tmp;
    data.set_user(login[from->socketDescriptor()], tmp);
}

void MyServer::fooNewMessage(QDataStream& getm)
{
    auto us = login[from->socketDescriptor()];
    size_t room;
    QString megs;
    getm >> room >> megs;
    if (!data.correct_room(room))
        return;
    buf.clear();
    QDataStream setm(&buf, QIODevice::WriteOnly);
    setm << (quint16)0 << (quint16)mode::new_in_meggase << room << us << megs;
    setm.device()->seek(0);
    setm << (quint16)(buf.size() - sizeof(quint16));
    for (auto &x: data.get_list_room(room))
    {
        if (x != us && socket_login.find(x) != socket_login.cend())
            table[socket_login[x]]->write(buf);
    }
}

void MyServer::fooGetRoomUser(QDataStream& getm)
{
    size_t room;
    getm >> room;
    setCmd(mode::get_room_user_in, room, data.get_list_room(room));
}

void MyServer::setData(QDataStream&)
{}

template <typename T, typename... Args>
void MyServer::setData(QDataStream& setm, T&& data, Args&&... args)
{
    setm << data;
    setData(setm, std::forward<Args>(args)...);
}

template <typename T, typename... Args>
void MyServer::setCmd(T&& val, Args&&... args)
{
    //out << "The answer for: " << login[from->socketDescriptor()] << " number: " << val << endl << flush;;
    buf.clear();
    QDataStream setm(&buf, QIODevice::WriteOnly);
    setm << (quint16)0 << (quint16)val;
    setData(setm, std::forward<Args>(args)...);
    setm.device()->seek(0);
    setm << (quint16)(buf.size() - sizeof(quint16));
    from->write(buf);
}
