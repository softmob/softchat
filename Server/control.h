#ifndef CONTROL_H
#define CONTROL_H
#include <QString>
#include <map>
#include "user.h"

class control
{
public:
    control();
    bool user_login(const QString& name, const QString& pass);
    bool user_create(const QString& name, const QString& pass, const user& oth = user());
    bool user_remove(const QString& name);
    user get_user(const QString& name);
    void set_user(const QString& name, const user& oth);
    size_t room_create(const QSet<QString>& oth);
    bool room_add(size_t ind, const QString& name);
    bool user_add(const QString& name, const QString& whom);
    bool user_del(const QString& name, const QString& whom);
    bool correct_room(size_t ind);
    QSet<QString>& get_list_room(size_t ind);
    QSet<QString>& get_user_list_friend(const QString& name);
    QSet<size_t>& get_user_list_room(const QString& name);
private:
    size_t counter_room;
    std::map<QString, QString> user_pass;
    std::map<QString, user> user_data;
    std::map<QString, QSet<QString>> friend_list;
    std::map<QString, QSet<size_t>> user_room;
    std::map<size_t, QSet<QString>> room_user;
    std::map<QString, size_t> new_room;
};

#endif // CONTROL_H
