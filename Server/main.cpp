#include <QCoreApplication>
#include "myserver.h"
#include "control.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    MyServer *server = new MyServer(nullptr, QHostAddress::LocalHost, 1234);
    return app.exec();
}
