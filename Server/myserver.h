#ifndef MYSERVER_H
#define MYSERVER_H

#include <QObject>
#include <QtNetwork>
#include <QString>
#include <QHash>
#include <set>
#include <utility>
#include <iostream>
#include <algorithm>
#include "mode.h"
#include "control.h"
#include "user.h"

#include "myserver.h"

class MyServer: public QObject
{
    Q_OBJECT
private:
    QHostAddress host;
    quint16 srv_port;
    QTcpServer *server;
    QTcpSocket *from;
    QByteArray buf;
public:
    MyServer(QObject *parent, QHostAddress type_addr, quint16 port);
private slots:
    void connection();
    void getMessage();
    void close();
private:
    void setstatus(const QString& str);
    void writeMapPoint();
     void fooUserDisconnect(QDataStream& getm);
    void fooLogin(QDataStream& getm);
    void fooRegistration(QDataStream& getm);
    void fooNewRoom(QDataStream& getm);
    void fooAddRoom(QDataStream& getm);
    void fooAddUser(QDataStream& getm);
    void fooAddUserYes(QDataStream& getm);
    void fooDelUser(QDataStream& getm);
    void fooGetRoomList(QDataStream& getm);
    void fooGetFriendList(QDataStream& getm);
    void fooNewMessage(QDataStream& getm);
    void fooGetRoomUser(QDataStream& getm);
    void fooGetUser(QDataStream& getm);
    void fooSetUser(QDataStream& getm);
    void setData(QDataStream&);
    template <typename T, typename... Args>
    void setData(QDataStream& setm, T&& data, Args&&... args);
    template <typename T, typename... Args>
    void setCmd(T&& val, Args&&... args);
private:
    std::map<qintptr, QTcpSocket*> table;
    std::map<qintptr, QString> login;
    std::map<QString, qintptr> socket_login;
    std::map<quint16, void(MyServer::*)(QDataStream&)> point_func;
    std::map<QString, bool> status;
    control data;
};

static QTextStream out(stdout);

#endif // MYSERVER_H
