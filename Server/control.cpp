#include "control.h"
#include <QStringList>
#include <QSet>
#include <QDebug>

bool control::user_login(const QString& name, const QString& pass)
{
    auto iter = user_pass.find(name);
    if (iter != user_pass.cend() && iter->second == pass)
        return true;
    else
        return false;
}

bool control::user_create(const QString& name, const QString& pass, const user& oth)
{
    if (user_data.find(name) != user_data.end())
        return false;
    user_pass[name] = pass;
    user_data[name] = oth;
    return true;
}

bool control::user_remove(const QString& name)
{
    if (user_data.find(name) == user_data.cend())
        return false;
    user_pass.erase(name);
    user_data.erase(name);
    return true;
}

user control::get_user(const QString& name)
{
    return user_data[name];
}

void control::set_user(const QString& name, const user& oth)
{
    user_data[name] = oth;
}

size_t control::room_create(const QSet<QString>& oth)
{
    QStringList list = oth.toList();
    auto str = list.join(",");
    qDebug() << str << endl;
    if (new_room.find(str) != new_room.cend())
        return new_room[str];
    room_user[++counter_room] = oth;
    new_room[str] = counter_room;
    qDebug() << counter_room;
    for (auto &x: oth)
    {
        user_room[x].insert(counter_room);
    }
    return counter_room;
}

bool control::room_add(size_t ind, const QString& name)
{
    if (room_user.find(ind) == room_user.cend())
        return false;
    QStringList list = room_user[ind].toList();
    new_room.erase(list.join(","));
    user_room[name].insert(ind);
    room_user[ind].insert(name);
    list.append(name);
    std::sort(list.begin(), list.end());
    new_room[list.join(",")] = ind;
    return true;
}

bool control::user_add(const QString& name, const QString& whom)
{
    if (user_data.find(name) == user_data.cend() || user_data.find(whom) == user_data.cend())
        return false;
    friend_list[name].insert(whom);
    return true;
}

bool control::user_del(const QString& name, const QString& whom)
{
    if (user_data.find(name) == user_data.cend() || user_data.find(whom) == user_data.cend())
        return false;
    auto us =  friend_list.find(name)->second;
    if (us.find(whom) == us.constEnd())
        return false;
    friend_list[name].remove(whom);
    return true;
}

bool control::correct_room(size_t ind)
{
    return room_user.find(ind) != room_user.end();
}

QSet<QString>& control::get_list_room(size_t ind)
{
    return room_user[ind];
}

QSet<QString>& control::get_user_list_friend(const QString& name)
{
    return friend_list[name];
}

QSet<size_t>& control::get_user_list_room(const QString& name)
{
    return user_room[name];
}

control::control()
{
    for (auto &x: {"1", "2", "3", "4", "5", "admin"})
        user_pass[x] = x, user_data[x];
}
