#ifndef USER_H
#define USER_H
#include <QString>
#include <QDataStream>

struct user
{
    QString name;
    size_t age;
    bool gender;
};

inline QDataStream& operator <<(QDataStream& stream, const user& oth)
{
    stream << oth.age << oth.gender << oth.name;
    return stream;
}

inline QDataStream& operator >>(QDataStream& stream, user& oth)
{
    stream >> oth.age >> oth.gender >> oth.name;
    return stream;
}

#endif // USER_H
